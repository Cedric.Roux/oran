#ifndef _BUFFERS_H_
#define _BUFFERS_H_

#define SUBFRAMES_PER_FRAME 10
#define MAX_SLOTS_PER_SUBFRAME 2
#define SYMBOLS_PER_SLOT 14

typedef enum {
  BUFFER_ACCESS_BLOCKING,
  BUFFER_ACCESS_NO_WAIT
} buffer_access_mode_t;

#include "sync.h"

#include <stdbool.h>

typedef struct {
  bool has_data[SUBFRAMES_PER_FRAME][MAX_SLOTS_PER_SUBFRAME][SYMBOLS_PER_SLOT];
  int rb_count[SUBFRAMES_PER_FRAME][MAX_SLOTS_PER_SUBFRAME][SYMBOLS_PER_SLOT];
  int slots_per_subframe;
  int symbols_per_slot;
  int nb_rb;
  char *data;    /* big array of size 10 * slots_per_subframe * symbols_per_slot * nb_rb * 12 * sizeof(uint16_t)*2 */
  sync_config_t sync;

  /* stats */
  int late_packets;
} buffer_config_t;

buffer_config_t *new_buffer(int slots_per_subframe, int symbols_per_slot, int nb_rb);
void free_buffer(buffer_config_t *c);

void buffer_set_has_data(buffer_config_t *b, int subframe, int slot, int symbol);

char *buffer_get_data(buffer_config_t *b, int subframe, int slot, int symbol, buffer_access_mode_t mode);
char *buffer_put_data(buffer_config_t *b, int subframe, int slot, int symbol, buffer_access_mode_t mode);
void buffer_release_data(buffer_config_t *b, int subframe, int slot, int symbol);
void buffer_commit_data(buffer_config_t *b, int subframe, int slot, int symbol, int rb_count);

#endif /* _BUFFERS_H_ */

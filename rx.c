#include "rx.h"

#include <stdio.h>
#include <sys/socket.h>

#include "utils.h"

void *rx_thread(void *_)
{
  processor_config_t *p = _;
  char buf[10000];
  depacket_state_t s;

  while (1) {
    struct sockaddr addr;
    socklen_t addr_len = sizeof(addr);
    int r = recvfrom(p->socket, buf, 10000, 0, &addr, &addr_len);
    if (r == -1) break;
    if (depacketize(&p->cfg, buf, r, &s) != 0)
      continue;

//    printf("recv type %d %d.%d.%d.%d %d.%d data", s.type, s.frame, s.subframe, s.slot, s.symbol, s.rb_start, s.rb_start + s.rb_count - 1);
//    for (int i = 0; i < 20; i++) printf(" %2.2x", (unsigned char)s.data[i]);
//    printf("\n");

    if (s.type == 0) {
      char *b = buffer_put_data(p->ul, s.subframe, s.slot, s.symbol, BUFFER_ACCESS_NO_WAIT);
//printf("YO buffer_put_data %d.%d.%d b %p\n", s.subframe, s.slot, s.symbol, b);
      if (b != NULL) {
        buffer_commit_data(p->ul, s.subframe, s.slot, s.symbol, s.rb_count);
      }
    }
  }

  fprintf(stderr, "rx thread terminated\n");
  return 0;
}

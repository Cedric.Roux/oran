#ifndef _TIME_UTIL_H_
#define _TIME_UTIL_H_

#include <stdint.h>

typedef struct {
  uint64_t second;
  uint32_t nanosecond;
} gps_time_t;

typedef gps_time_t ptp_time_t;

void get_gps_time(gps_time_t *now);
gps_time_t sub_nanosecond_gps_time(gps_time_t t, int nanosec);
gps_time_t add_nanosecond_gps_time(gps_time_t t, int nanosec);
gps_time_t timediff(gps_time_t a, gps_time_t b);
void gps_nanosleep(gps_time_t abstime);
void get_gps_start_time(gps_time_t *start);

void gps_to_ptp(ptp_time_t *out, gps_time_t *in);

#endif /* _TIME_UTIL_H_ */

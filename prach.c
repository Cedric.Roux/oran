#include "prach.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <net/if.h>

#include <errno.h>

#include <pthread.h>

#include "utils.h"
#include "nr.h"

#define ECPRI_PROTOCOL 0xae, 0xfe

void transmit_prach_c_plane(processor_config_t *p, packet_state_t *st, int frame, int subframe, int slot, int symbol)
{
  gps_time_t t;
  get_gps_time(&t);
//  printf("C prach %ld.%9.9d %d.%d.%d.%d\n", t.second, t.nanosecond, frame, subframe, slot, symbol);

  char b[9000];
  int len;

  st->frame = frame;
  st->subframe = subframe;
  st->slot = slot;

  len = make_cp_prach(&p->cfg, st, b, sizeof(b));
  if (len == -1) exit(1);

  st->seq_id_cp_prach++;

  unsigned char prot[2] = { ECPRI_PROTOCOL };
  unsigned short ecpri_prot = (prot[1] << 8) | prot[0];

  struct sockaddr_ll addr = { 0 };
  addr.sll_family = AF_PACKET;
  addr.sll_protocol = ecpri_prot;
  addr.sll_ifindex = p->interface_index;
  errno = 0;

  if (sendto(p->socket, b, len, 0, (struct sockaddr *)&addr, sizeof(addr)) != len) { perror("send"); }
}

#define NEXT_SYMBOL(symbol, slot, subframe, frame) do { \
    symbol++;                                           \
    if (symbol == n_symb_slot) {                        \
      symbol = 0;                                       \
      slot++;                                           \
      if (slot == n_slot_subframe_mu) {                 \
        slot = 0;                                       \
        subframe++;                                     \
        if (subframe == 10) {                           \
          subframe = 0;                                 \
          frame++;                                      \
          if (frame == 1024) frame = 0;                 \
        }                                               \
      }                                                 \
    }                                                   \
  } while (0)

void *prach_thread(void *_)
{
  processor_config_t *p = _;
  packet_state_t st;
  int mu = p->mu;
  int frame = 0;
  int subframe = 0;
  int slot = 0;
  int symbol = 0;
  int n_slot_subframe_mu = 1 << mu;
  int n_symb_slot = 14;
  int delta_start[n_slot_subframe_mu * n_symb_slot];

  memset(&st, 0, sizeof(st));

  set_realtime_thread();

  /* compute delta_start[] */
  int prev_start = 0;
  int cumulated_delay = 0;
  for (int i = 1; i < n_slot_subframe_mu * n_symb_slot; i++) {
    int start = t_start_l_mu(mu, i, 0);
    delta_start[i] = start - prev_start;
    cumulated_delay += delta_start[i];
    prev_start = start;
  }
  /* delta_start for symbol 0 is what remains from a total of 1ms */
  delta_start[0] = 1000000 - cumulated_delay;
  printf("%d\n", cumulated_delay);
  for (int i = 0; i < n_slot_subframe_mu * n_symb_slot; i++)
    printf("symbol %d start %g %d (delta start %d)\n",
           i, t_start_l_mu(mu, i, 0), (int)t_start_l_mu(mu, i, 0),
           delta_start[i]);

  gps_time_t now;
  get_gps_time(&now);

  gps_time_t next_gps_time = p->gps_start_time;
  printf("current gps %ld %d\n", now.second, now.nanosecond);
  printf("next gps %ld %d\n", next_gps_time.second, next_gps_time.nanosecond);

  gps_time_t diff = timediff(next_gps_time, now);
  printf("starting in %ld seconds and %d nanoseconds\n", diff.second, diff.nanosecond);
  fprintf(stderr, "starting in %ld seconds and %d nanoseconds\n", diff.second, diff.nanosecond); fflush(stderr);

  while (1) {
    gps_time_t transmit_time;
    int is_prach_slot = p->cfg.prach_slot[subframe * p->cfg.slots_per_subframe + slot];
    int advance;
    if (is_prach_slot && symbol == 0) {
      /* transmit C-Plane for current slot */
      advance = p->t2a_max_cp_ul;
      transmit_time = sub_nanosecond_gps_time(next_gps_time, advance);
      /* deal with TA */
      /* 25600 samples N_TA_offset = 25600 / (480kHz * 4096) sec = 13021 nanosec */
      transmit_time = sub_nanosecond_gps_time(transmit_time, 13021);
//{gps_time_t now; get_gps_time(&now); printf("PRACH needs to transmit C at %ld.%9.9d (now %ld.%9.9d)\n", transmit_time.second, transmit_time.nanosecond, now.second, now.nanosecond); }
      gps_nanosleep(transmit_time);
      transmit_prach_c_plane(p, &st, frame, subframe, slot, symbol);
    }

    NEXT_SYMBOL(symbol, slot, subframe, frame);
    next_gps_time = add_nanosecond_gps_time(next_gps_time, delta_start[slot * n_symb_slot + symbol]);
//    if (symbol == 0)
//      printf(" next %ld.%9.9d\n", next_gps_time.second, next_gps_time.nanosecond);
  }

  return 0;
}

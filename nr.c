#include "nr.h"

#include <stdio.h>
#include <stdlib.h>

double n_cp_l_mu(int mu, int l, int is_extended)
{
  double K = 64;

  if (is_extended) exit(1);

  return l == 0 || l == 7 * (1 << mu) ? 144. * K / (1 << mu) + 16 * K
                                      : 144. * K / (1 << mu);
}

static double _t_start_l_mu(int mu, int l, int is_extended)
{
  double K = 64;
  double N_u_mu = 2048. * K / (1 << mu);

  if (l == 0) return 0;

  double prev = _t_start_l_mu(mu, l-1, is_extended);

  return prev + (N_u_mu + n_cp_l_mu(mu, l - 1, is_extended));
}

double t_start_l_mu(int mu, int l, int is_extended)
{
  double DeltaF_max = 480000; 
  double Nf = 4096;
  double Tc_ns = 1000000000. / (DeltaF_max * Nf);
  return _t_start_l_mu(mu, l, is_extended) * Tc_ns;
}

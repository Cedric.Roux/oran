#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdint.h>

#include "packetize.h"
#include "time_util.h"
#include "buffers.h"

typedef struct {
  int socket;
  int interface_index;

  int mu;
  int mtu;

  int tadv_cp_dl;          //=125 # in us

  // Reception Window C-plane DL
  int t2a_min_cp_dl;       //=285 # 285.42us
  int t2a_max_cp_dl;       //=429 # 428.12us

  // Reception Window C-plane UL
  int t2a_min_cp_ul;       //=285 # 285.42us
  int t2a_max_cp_ul;       //=429 # 428.12us

  // Reception Window U-plane
  int t2a_min_up;          //=125  # 71.35in us
  int t2a_max_up;          //=428 # 428.12us

  // Transmission Window
  int ta3_min;             //=130 # in us 
  int ta3_max;             //=170 # in us 

  // O-DU Settings
  // C-plane
  // Transmission Window Fast C-plane DL
  int t1a_min_cp_dl;       //=285
  int t1a_max_cp_dl;       //=470

  // Transmission Window Fast C-plane UL
  int t1a_min_cp_ul;       //=285
  int t1a_max_cp_ul;       //=429

  // U-plane
  // Transmission Window
  int t1a_min_up;          //=125  #71 + 25 us
  int t1a_max_up;          //=350 #71 + 25 us

  // Reception Window
  int ta4_min;             //=110  # in us 
  int ta4_max;             //=180 # in us 

  packet_config_t cfg;

  gps_time_t gps_start_time;

  buffer_config_t *dl;
  buffer_config_t *ul;
} processor_config_t;

void new_thread(void *(*f)(void *), void *data);
void set_realtime_thread(void);
void get_mac_address(unsigned char *out, const char *in);

#endif /* _UTILS_H_ */

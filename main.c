#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h> /* for sleep() */
#include <arpa/inet.h>

#include "dl.h"
#include "ul.h"
#include "prach.h"
#include "rx.h"
#include "utils.h"
#include "packetize.h"

#define SRC_MAC_ADDRESS "76:76:64:6e:00:01"
#define DST_MAC_ADDRESS "98:ae:71:01:c5:eb"

#define ECPRI_PROTOCOL 0xae, 0xfe
//#define DST_ADDR 0x98, 0xae, 0x71, 0x01, 0x64, 0xd8
//#define DST_ADDR 0x98, 0xae, 0x71, 0x01, 0xc5, 0xeb
//#define SRC_ADDR  0x00, 0x11, 0x22, 0x33, 0x44, 0x66
//#define SRC_ADDR  0x76, 0x76, 0x64, 0x6e, 0x00, 0x01

#if 0
int create_packet(unsigned char *out,
        unsigned char *src, unsigned char *dst)
{
  int ret = 0;
  unsigned char prot[2] = { ECPRI_PROTOCOL };

unsigned char data[] = {
0x10, 0x02,
  0x00, 0x14, 0x00, 0x01, 0x00, 0x80, 0x10, 0x00,
  0x00, 0x00, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0xff, 0xfe, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00};

  memcpy(out+ret, dst, 6); ret += 6;
  memcpy(out+ret, src, 6); ret += 6;

#if 0
  /* vlan */
  int vlan = 1;
  unsigned char vl[4] = { 0x81, 0x00, 0x00, 0x00 };
  vl[2] = vlan / 256;
  vl[3] = vlan % 256;
  memcpy(out+ret, vl, 4); ret += 4;
#endif

  memcpy(out+ret, prot, 2); ret += 2;

  memcpy(out+ret, data, sizeof(data)); ret += sizeof(data);

  return ret;
}
#else
int create_packet(unsigned char *out,
        unsigned char *src_mac_address, unsigned char *dst_mac_address)
{
  packet_config_t cfg;
  packet_state_t st;

  memcpy(cfg.eth_dst, dst_mac_address, 6);
  memcpy(cfg.eth_src, src_mac_address, 6);
  memcpy(cfg.eth_type, (char[]){ ECPRI_PROTOCOL }, 2);
  cfg.vlan = -1; //-1;
  cfg.eaxc_id_dl = 0;
  cfg.eaxc_id_ul = 0;

  int start_dl_symbol[20] = {
    0, 0, 0, 0, -1,
    0, 0, 0, 0, -1,
    0, 0, 0, 0, -1,
    0, 0, 0, 0, -1,
  };

  int num_dl_symbol[20] = {
    14, 14, 14, 6, -1,
    14, 14, 14, 6, -1,
    14, 14, 14, 6, -1,
    14, 14, 14, 6, -1,
  };

  cfg.start_dl_symbol = start_dl_symbol;
  cfg.num_dl_symbol = num_dl_symbol;

  int start_ul_symbol[20] = {
    -1, -1, -1, 10, 0,
    -1, -1, -1, 10, 0,
    -1, -1, -1, 10, 0,
    -1, -1, -1, 10, 0,
  };

  int num_ul_symbol[20] = {
    -1, -1, -1, 4, 14,
    -1, -1, -1, 4, 14,
    -1, -1, -1, 4, 14,
    -1, -1, -1, 4, 14,
  };

  cfg.start_ul_symbol = start_ul_symbol;
  cfg.num_ul_symbol = num_ul_symbol;

  cfg.slots_per_subframe = 2;

  st.seq_id_cp_dl = 0;
  st.seq_id_cp_ul = 0;
  st.frame = 0;
  st.subframe = 2;
  st.slot = 0;

  return make_cp_ul(&cfg, &st, (char *)out, 10000);
}
#endif

void init(packet_config_t *cfg,
        unsigned char *src_mac_address, unsigned char *dst_mac_address)
{
  memcpy(cfg->eth_dst, dst_mac_address, 6);
  memcpy(cfg->eth_src, src_mac_address, 6);
  memcpy(cfg->eth_type, (char[]){ ECPRI_PROTOCOL }, 2);
  cfg->vlan = -1; //-1;
  cfg->eaxc_id_dl = 0;
  cfg->eaxc_id_ul = 0;
  cfg->eaxc_id_prach = 2;

  static int start_dl_symbol[20] = {
    0, 0, 0, 0, -1,
    0, 0, 0, 0, -1,
    0, 0, 0, 0, -1,
    0, 0, 0, 0, -1,
  };

  static int num_dl_symbol[20] = {
    14, 14, 14, 6, -1,
    14, 14, 14, 6, -1,
    14, 14, 14, 6, -1,
    14, 14, 14, 6, -1,
  };

  cfg->start_dl_symbol = start_dl_symbol;
  cfg->num_dl_symbol = num_dl_symbol;

  static int start_ul_symbol[20] = {
    -1, -1, -1, 10, 0,
    -1, -1, -1, 10, 0,
    -1, -1, -1, 10, 0,
    -1, -1, -1, 10, 0,
  };

  static int num_ul_symbol[20] = {
    -1, -1, -1, 4, 14,
    -1, -1, -1, 4, 14,
    -1, -1, -1, 4, 14,
    -1, -1, -1, 4, 14,
  };

  cfg->start_ul_symbol = start_ul_symbol;
  cfg->num_ul_symbol = num_ul_symbol;

  static int prach_slot[20] = {
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 1,
  };

  cfg->prach_slot = prach_slot;

  cfg->slots_per_subframe = 2;
}

void init_buffers(processor_config_t *p)
{
  int subframe, slot, symbol;

printf("dl config:\n");
  /* dl */
  p->dl = new_buffer(2, 14, 273);
  for (subframe = 0; subframe < 10; subframe++)
    for (slot = 0; slot < 2; slot++) {
      int sl = subframe * 2 + slot;
      int start = p->cfg.start_dl_symbol[sl];
      int end = p->cfg.start_dl_symbol[sl] + p->cfg.num_dl_symbol[sl] - 1;
      for (symbol = start; symbol <= end; symbol ++)
        buffer_set_has_data(p->dl, subframe, slot, symbol);
    }

printf("ul config:\n");
  /* ul */
  p->ul = new_buffer(2, 14, 273);
  for (subframe = 0; subframe < 10; subframe++)
    for (slot = 0; slot < 2; slot++) {
      int sl = subframe * 2 + slot;
      int start = p->cfg.start_ul_symbol[sl];
      int end = p->cfg.start_ul_symbol[sl] + p->cfg.num_ul_symbol[sl] - 1;
      for (symbol = start; symbol <= end; symbol ++)
        buffer_set_has_data(p->ul, subframe, slot, symbol);
    }
}

void usage(void)
{
  printf("options:\n");
  printf("    -iface <interface to use>\n");
  printf("    -dmac <dest mac address, default %s\n>", DST_MAC_ADDRESS);
  printf("    -smac <source mac address, default %s\n>", SRC_MAC_ADDRESS);
  printf("    -test\n");
  printf("        test mode: send 1 CP UL packet, print received packets\n");
  printf("    -delay\n");
  printf("        ecpri delay measurement\n");
  printf("    -replay [file]\n");
  exit(0);
}

int main(int n, char **v)
{
  processor_config_t p;
  int test_mode = 0;
  int delay_mode = 0;
  char *replay_file = NULL;
  unsigned char src_mac_address[6];
  unsigned char dst_mac_address[6];

  get_mac_address(src_mac_address, SRC_MAC_ADDRESS);
  get_mac_address(dst_mac_address, DST_MAC_ADDRESS);

  p.mu = 1;
  p.mtu = 9000;
  /* DL timings */
  p.tadv_cp_dl    = 125 * 1000;
  p.t1a_max_cp_dl = 470 * 1000;
  p.t1a_max_up    = 350 * 1000;
  /* UL timings */
  p.t1a_max_cp_ul = 429 * 1000;
  /* PRACH timings */
  p.t2a_max_cp_ul = 429 * 1000;

  unsigned char prot[2] = { ECPRI_PROTOCOL };
  char *iface = NULL;
  int s;
  int i;
  int interface_index;

  unsigned short ecpri_prot = (prot[1] << 8) | prot[0];

  for (i = 1; i < n; i++) {
    if (!strcmp(v[i], "-test")) { test_mode = 1; continue; }
    if (!strcmp(v[i], "-delay")) { delay_mode = 1; continue; }
    if (!strcmp(v[i], "-iface")) { if (i > n-2) usage(); iface = v[++i]; continue; }
    if (!strcmp(v[i], "-replay")) { if (i > n-2) usage(); replay_file = v[++i]; continue; }
    if (!strcmp(v[i], "-dmac")) { if (i > n-2) usage(); get_mac_address(dst_mac_address, v[++i]); continue; }
    if (!strcmp(v[i], "-smac")) { if (i > n-2) usage(); get_mac_address(src_mac_address, v[++i]); continue; }
    usage();
  }
  if (iface == NULL) { printf("give an interface (option -iface)\n"); exit(1); }

  init(&p.cfg, src_mac_address, dst_mac_address);

  s = socket(AF_PACKET, SOCK_RAW, ecpri_prot);
  if (s == -1) { perror("socket"); exit(1); }

  struct ifreq itf;
  if (strlen(iface) > IFNAMSIZ-1) { printf("interface name %s too long\n", iface); exit(1); }
  strcpy(itf.ifr_name, iface);

  if (ioctl(s, SIOCGIFINDEX, &itf) == -1) { perror("ioctl"); exit(1); }
  interface_index = itf.ifr_ifindex;
  printf("interface %s has index %d\n", iface, interface_index);

  struct sockaddr_ll addr;
  addr.sll_family = AF_PACKET;
  addr.sll_protocol = htons(0xaefe);
  addr.sll_ifindex = interface_index;

  if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) == -1) { perror("bind"); exit(1); }

  p.socket = s;
  p.interface_index = interface_index;

  get_gps_start_time(&p.gps_start_time);
  //get_gps_time(&p.gps_start_time);

  if (delay_mode) {
    unsigned char buf[10000];
    gps_time_t now;
    get_gps_time(&now);
    int len = make_oneway_delay(&p.cfg, (char *)buf, 10000, &now);
    struct sockaddr_ll addr;
    addr.sll_family = AF_PACKET;
    addr.sll_protocol = ecpri_prot;
    addr.sll_ifindex = interface_index;
for (i = 0; i < len; i++) printf(" %2.2x", buf[i]);
printf("\n");
//len = 3000;
    if (sendto(s, buf, len, 0, (struct sockaddr *)&addr, sizeof(addr)) != len) { perror("send"); }

    while (1) {
      int r = recv(s, buf, 10000, 0);
      printf("recv[%d]:", r);
r=12;
      for (i = 0; i < r; i++) printf(" %2.2x", buf[i]);
//printf("    ");
      printf("\n");
    }
  }

  if (test_mode) {
    unsigned char buf[10000];
    int len = create_packet(buf, src_mac_address, dst_mac_address);
    struct sockaddr_ll addr;
    addr.sll_family = AF_PACKET;
    addr.sll_protocol = ecpri_prot;
    addr.sll_ifindex = interface_index;
for (i = 0; i < len; i++) printf(" %2.2x", buf[i]);
printf("\n");
//len = 3000;
    if (sendto(s, buf, len, 0, (struct sockaddr *)&addr, sizeof(addr)) != len) { perror("send"); }

    while (1) {
      int r = recv(s, buf, 10000, 0);
      printf("recv[%d]:", r);
r=12;
      for (i = 0; i < r; i++) printf(" %2.2x", buf[i]);
//printf("    ");
      printf("\n");
    }
  }

  init_buffers(&p);

  if (replay_file != NULL) {
    char (*has_data)[10][2][14];
    char (*data)[10][2][14][273*12*4];
    int frame, subframe, slot, symbol;

    has_data = calloc(1, 1024*10*2*14); if (has_data == NULL) abort();
    data = calloc(1, (unsigned long)1024*10*2*14*273*12*4); if (data == NULL) abort();

    FILE *in = fopen(replay_file, "r"); if (in == NULL) { perror(replay_file); exit(1); }
    while (1) {
      if (fread(&frame, 4, 1, in) != 1) break;
      if (fread(&subframe, 4, 1, in) != 1) { printf("%s: bad input file\n", replay_file); exit(1); }
      if (fread(&slot, 4, 1, in) != 1) { printf("%s: bad input file\n", replay_file); exit(1); }
      if (fread(&symbol, 4, 1, in) != 1) { printf("%s: bad input file\n", replay_file); exit(1); }
      has_data[frame][subframe][slot][symbol] = 1;
      if (fread(data[frame][subframe][slot][symbol], 273*12*4, 1, in) != 1) { printf("%s: bad input file\n", replay_file); exit(1); }
    }
    fclose(in);

    new_thread(dl_thread, &p);

    while (1) {
      for (frame = 0; frame < 1024; frame++)
      for (subframe = 0; subframe < 10; subframe++)
      for (slot = 0; slot < 2; slot++)
      for (symbol = 0; symbol < 14; symbol++)
        if (has_data[frame][subframe][slot][symbol]) {
          char *b = buffer_put_data(p.dl, subframe, slot, symbol, BUFFER_ACCESS_BLOCKING);
          if (b == NULL) { printf("fatal: buffer_put_data returns NULL\n"); exit(1); }
          memcpy(b, data[frame][subframe][slot][symbol], 273 * 12 * 4);
          buffer_commit_data(p.dl, subframe, slot, symbol, 273);
        }
    }
  }

  new_thread(dl_thread, &p);
  new_thread(ul_thread, &p);
  new_thread(prach_thread, &p);
  new_thread(rx_thread, &p);

  while (1) pause();

  return 0;
}

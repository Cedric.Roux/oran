#include "sync.h"

#include <stdio.h>
#include <stdlib.h>

void sync_init(sync_config_t *s)
{
  if (pthread_mutex_init(&s->m, NULL)) {
    printf("fatal: pthread_mutex_init fails\n");
    exit(1);
  }
  if (pthread_cond_init(&s->c, NULL)) {
    printf("fatal: pthread_cond_init fails\n");
    exit(1);
  }
}

void lock(sync_config_t *s)
{
  if (pthread_mutex_lock(&s->m)) {
    printf("fatal: pthread_mutex_lock fails\n");
    exit(1);
  }
}

void unlock(sync_config_t *s)
{
  if (pthread_mutex_unlock(&s->m)) {
    printf("fatal: pthread_mutex_unlock fails\n");
    exit(1);
  }
}

void sync_wait(sync_config_t *s)
{
  if (pthread_cond_wait(&s->c, &s->m)) {
    printf("fatal: pthread_cond_wait fails\n");
    exit(1);
  }
}

void sync_notify(sync_config_t *s)
{
  if (pthread_cond_broadcast(&s->c)) {
    printf("fatal: pthread_cond_wait fails\n");
    exit(1);
  }
}

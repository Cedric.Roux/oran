#include "utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>

void new_thread(void *(*f)(void *), void *data)
{
  pthread_t t;
  pthread_attr_t att;

  if (pthread_attr_init(&att))
    { fprintf(stderr, "pthread_attr_init err\n"); exit(1); }
#if 0
  if (pthread_attr_setdetachstate(&att, PTHREAD_CREATE_DETACHED))
    { fprintf(stderr, "pthread_attr_setdetachstate err\n"); exit(1); }
#endif
  if (pthread_create(&t, &att, f, data))
    { fprintf(stderr, "pthread_create err\n"); exit(1); }
  if (pthread_attr_destroy(&att))
    { fprintf(stderr, "pthread_attr_destroy err\n"); exit(1); }
}

void set_realtime_thread(void)
{
  struct sched_param p;
  p.sched_priority = sched_get_priority_min(SCHED_FIFO);
  if (sched_setscheduler(0, SCHED_FIFO, &p)) {
    fprintf(stderr, "fatal: error setting thread to realtime\n");
    exit(1);
  }
}

static int hex(int c)
{
  if (c >= '0' && c <= '9') return c - '0';
  if (c >= 'A' && c <= 'F') return c + 10 - 'A';
  if (c >= 'a' && c <= 'f') return c + 10 - 'a';
  return -1;
}

void get_mac_address(unsigned char *out, const char *in)
{
  int i;

  if (strlen(in) != 3*6 - 1)
  for (i = 0; i < 6 - 1; i++) if (in[i*3+2] != ':') goto err;
  for (i = 0; i < 6; i++) {
    int a = hex((unsigned char)in[i * 3]);
    int b = hex((unsigned char)in[i * 3 + 1]);
    if (a == -1 || b == -1) goto err;
    out[i] = a * 16 + b;
  }

  return;

err:
  fprintf(stderr, "bad mac %s\n", in);
  exit(1);
}

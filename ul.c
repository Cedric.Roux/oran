#include "ul.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <net/if.h>

#include <errno.h>

#include <pthread.h>

#include "utils.h"
#include "nr.h"

#define SRC_ADDR  0x00, 0x11, 0x22, 0x33, 0x44, 0x66
#define DST_ADDR 0x98, 0xae, 0x71, 0x01, 0x64, 0xd8
#define ECPRI_PROTOCOL 0xae, 0xfe

#if 0
void transmit_ul_c_plane(int socket, int interface_index, int frame, int subframe, int slot, int symbol)
{
  gps_time_t t;
  get_gps_time(&t);
//  printf("UL control sent at %ld.%9.9d %d.%d.%d.%d\n", t.second, t.nanosecond, frame, subframe, slot, symbol);

  unsigned char b[] = {
    0x98, 0xae, 0x71, 0x01, 0x64, 0xd8, 0x00, 0x11,
    0x22, 0x33, 0x44, 0x66, 0xae, 0xfe,
0x10, 0x02,
  0x00, 0x14, 0x00, 0x00, 0x05, 0x80, 0x10, 0x00,
  0x20, 0x00, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0xff, 0xfe, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00};
#if 0
  /* for dl */
 0x10, 0x02,
    0x00, 0x14, 0x00, 0x00, 0x00, 0x80, 0x90, 0x00,
    0x00, 0x00, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0xff, 0xfe, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00
  };
#endif

  unsigned char dst[6] = { DST_ADDR };
  unsigned char src[6] = { SRC_ADDR };

  memcpy(b, dst, 6);
  memcpy(b+6, src, 6);

  static int seq_id = 0;
  b[20] = seq_id;
  seq_id++;

  b[23] = frame;
  uint16_t sss = (subframe << 12) | (slot << 6) | symbol;
  b[24] = sss >> 8;
  b[25] = sss & 255;

  unsigned char prot[2] = { ECPRI_PROTOCOL };
  unsigned short ecpri_prot = (prot[1] << 8) | prot[0];

  struct sockaddr_ll addr = { 0 };
  addr.sll_family = AF_PACKET;
  addr.sll_protocol = ecpri_prot;
  addr.sll_ifindex = interface_index;
  errno = 0;

  if (sendto(socket, b, sizeof(b), 0, (struct sockaddr *)&addr, sizeof(addr)) != sizeof(b)) { perror("send"); }
}
#else
void transmit_ul_c_plane(processor_config_t *p, packet_state_t *st, int frame, int subframe, int slot, int symbol)
{
  gps_time_t t;
  get_gps_time(&t);
//  printf("UL control sent at %ld.%9.9d %d.%d.%d.%d\n", t.second, t.nanosecond, frame, subframe, slot, symbol);

  char b[9000];
  int len;

  st->frame = frame;
  st->subframe = subframe;
  st->slot = slot;

  len = make_cp_ul(&p->cfg, st, b, sizeof(b));
  if (len == -1) exit(1);

  st->seq_id_cp_ul++;

  unsigned char prot[2] = { ECPRI_PROTOCOL };
  unsigned short ecpri_prot = (prot[1] << 8) | prot[0];

  struct sockaddr_ll addr = { 0 };
  addr.sll_family = AF_PACKET;
  addr.sll_protocol = ecpri_prot;
  addr.sll_ifindex = p->interface_index;
  errno = 0;

  if (sendto(p->socket, b, len, 0, (struct sockaddr *)&addr, sizeof(addr)) != len) { perror("send"); }
}
#endif

#define NEXT_SYMBOL(symbol, slot, subframe, frame) do { \
    symbol++;                                           \
    if (symbol == n_symb_slot) {                        \
      symbol = 0;                                       \
      slot++;                                           \
      if (slot == n_slot_subframe_mu) {                 \
        slot = 0;                                       \
        subframe++;                                     \
        if (subframe == 10) {                           \
          subframe = 0;                                 \
          frame++;                                      \
          if (frame == 1024) frame = 0;                 \
        }                                               \
      }                                                 \
    }                                                   \
  } while (0)

void *ul_thread(void *_)
{
  processor_config_t *p = _;
  packet_state_t st;
  int mu = p->mu;
  int frame = 0;
  int subframe = 0;
  int slot = 0;
  int symbol = 0;
  int n_slot_subframe_mu = 1 << mu;
  int n_symb_slot = 14;
  int delta_start[n_slot_subframe_mu * n_symb_slot];

  memset(&st, 0, sizeof(st));

  set_realtime_thread();

  /* compute delta_start[] */
  int prev_start = 0;
  int cumulated_delay = 0;
  for (int i = 1; i < n_slot_subframe_mu * n_symb_slot; i++) {
    int start = t_start_l_mu(mu, i, 0);
    delta_start[i] = start - prev_start;
    cumulated_delay += delta_start[i];
    prev_start = start;
  }
  /* delta_start for symbol 0 is what remains from a total of 1ms */
  delta_start[0] = 1000000 - cumulated_delay;
//  printf("%d\n", cumulated_delay);
  for (int i = 0; i < n_slot_subframe_mu * n_symb_slot; i++)
    printf("symbol %d start %g %d (delta start %d)\n",
           i, t_start_l_mu(mu, i, 0), (int)t_start_l_mu(mu, i, 0),
           delta_start[i]);

  gps_time_t now;
  get_gps_time(&now);

  gps_time_t next_gps_time = p->gps_start_time;
  printf("current gps %ld %d\n", now.second, now.nanosecond);
  printf("next gps %ld %d\n", next_gps_time.second, next_gps_time.nanosecond);

  gps_time_t diff = timediff(next_gps_time, now);
  printf("starting in %ld seconds and %d nanoseconds\n", diff.second, diff.nanosecond);
  fprintf(stderr, "starting in %ld seconds and %d nanoseconds\n", diff.second, diff.nanosecond); fflush(stderr);

  while (1) {
    gps_time_t transmit_time;
    int start_symbol = p->cfg.start_ul_symbol[subframe * p->cfg.slots_per_subframe + slot];
    int advance;
    if (symbol == start_symbol) {
      /* transmit C-Plane for current slot */
      advance = p->t1a_max_cp_ul;
      transmit_time = sub_nanosecond_gps_time(next_gps_time, advance);
      /* deal with TA */
      /* 25600 samples N_TA_offset = 25600 / (480kHz * 4096) sec = 13021 nanosec */
      transmit_time = sub_nanosecond_gps_time(transmit_time, 13021);
//{gps_time_t now; get_gps_time(&now); printf("UL needs to transmit C at %ld.%9.9d (now %ld.%9.9d)\n", transmit_time.second, transmit_time.nanosecond, now.second, now.nanosecond); }
      gps_nanosleep(transmit_time);
      transmit_ul_c_plane(p, &st, frame, subframe, slot, symbol);
    }

    NEXT_SYMBOL(symbol, slot, subframe, frame);
    next_gps_time = add_nanosecond_gps_time(next_gps_time, delta_start[slot * n_symb_slot + symbol]);
//    if (symbol == 0)
//      printf("UL next %d.%d.%d.%d %ld.%9.9d\n", frame, subframe, slot, symbol, next_gps_time.second, next_gps_time.nanosecond);
  }

  return 0;
}

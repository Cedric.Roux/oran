#ifndef _PACKETIZE_H_
#define _PACKETIZE_H_

typedef enum {
  COMPRESSION_NONE_16BITS,
} compression_t;

typedef enum {
  PACKET_ULSCH, PACKET_PRACH
} packet_type_t;

typedef struct {
  char eth_src[6];
  char eth_dst[6];
  char eth_type[2];
  int vlan;           /* -1 for no vlan/untagged vlan */

  int eaxc_id_dl;
  int eaxc_id_ul;
  int eaxc_id_prach;

  int *start_dl_symbol;   /* size of array: number of slots per subframe * number of subrames per frame */
  int *num_dl_symbol;     /* size of array: number of slots per subframe * number of subrames per frame */
  int *start_ul_symbol;   /* size of array: number of slots per subframe * number of subrames per frame */
  int *num_ul_symbol;     /* size of array: number of slots per subframe * number of subrames per frame */
  int *prach_slot;        /* size of array: number of slots per subframe * number of subrames per frame */
  int slots_per_subframe;

  compression_t compression_type;
} packet_config_t;

typedef struct {
  unsigned char seq_id_cp_dl;
  unsigned char seq_id_cp_ul;
  unsigned char seq_id_up_dl;
  unsigned char seq_id_cp_prach;
  int frame;
  int subframe;
  int slot;
} packet_state_t;

int make_cp_dl(packet_config_t *cfg, packet_state_t *st, char *out, int size);
int make_cp_ul(packet_config_t *cfg, packet_state_t *st, char *out, int size);
int make_up_dl(packet_config_t *cfg, packet_state_t *st, char *out, int size,
               char *iq, int rb_start, int rb_count, int symbol_id);
int make_cp_prach(packet_config_t *cfg, packet_state_t *st, char *out, int size);

#include "time_util.h"

int make_oneway_delay(packet_config_t *cfg, char *out, int size, gps_time_t *now);

typedef enum {
  PACKET_TYPE_UL, PACKET_TYPE_PRACH
} depacket_type_t;

typedef struct {
  depacket_type_t type;
  int frame;
  int subframe;
  int slot;
  int symbol;
  int rb_start;
  int rb_count;
  char *data;
} depacket_state_t;

/* returns 0 if a packet was processed, -1 if not */
int depacketize(packet_config_t *cfg, char *in, int size, depacket_state_t *s);

#endif /* _PACKETIZE_H_ */

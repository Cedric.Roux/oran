#include "buffers.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

buffer_config_t *new_buffer(int slots_per_subframe, int symbols_per_slot, int nb_rb)
{
  buffer_config_t *ret = calloc(1, sizeof(*ret));
  if (ret == NULL) {
    printf("out of memory\n");
    exit(1);
  }

  ret->slots_per_subframe = slots_per_subframe;
  ret->symbols_per_slot = symbols_per_slot;
  ret->nb_rb = nb_rb;

  int subframes_per_frame = 10;
  int re_per_rb = 12;
  int bytes_per_re = 4;

  ret->data = calloc(1, subframes_per_frame * slots_per_subframe * symbols_per_slot * nb_rb * re_per_rb * bytes_per_re);
  if (ret->data == NULL) {
    printf("out of memory\n");
    exit(1);
  }

  sync_init(&ret->sync);

  return ret;
}

void free_buffer(buffer_config_t *c)
{
  free(c->data);
  free(c);
}

void buffer_set_has_data(buffer_config_t *b, int subframe, int slot, int symbol)
{
//printf("set has_data %d.%d.%d\n", subframe, slot, symbol);
  b->has_data[subframe][slot][symbol] = true;
}

char *buffer_get_data(buffer_config_t *b, int subframe, int slot, int symbol, buffer_access_mode_t mode)
{
  lock(&b->sync);

  if (!b->has_data[subframe][slot][symbol]) {
    printf("warning: asking for %d.%d.%d but no data, returning NULL\n", subframe, slot, symbol);
    unlock(&b->sync);
    //return NULL;
    return (void *)1;
  }

  while (b->rb_count[subframe][slot][symbol] != b->nb_rb) {
    if (mode == BUFFER_ACCESS_NO_WAIT) {
//      printf("warning: asking for %d.%d.%d but no data ready (late), returning NULL\n", subframe, slot, symbol);
      b->late_packets++;
      unlock(&b->sync);
      return NULL;
    }
    sync_wait(&b->sync);
  }

  unlock(&b->sync);

  int re_per_rb = 12;
  int bytes_per_re = 4;

  char (*x)[b->slots_per_subframe][b->symbols_per_slot][b->nb_rb][re_per_rb][bytes_per_re] =
    (char (*)[b->slots_per_subframe][b->symbols_per_slot][b->nb_rb][re_per_rb][bytes_per_re])b->data;

//printf("Z sf %d slot %d symbol %d x %p &x %p x-b %d\n", subframe, slot, symbol, x[subframe][slot][symbol], &x[subframe][slot][symbol], (int)(((char *)&x[subframe][slot][symbol])-(char *)b->data));
  return (char *)&x[subframe][slot][symbol];
}

char *buffer_put_data(buffer_config_t *b, int subframe, int slot, int symbol, buffer_access_mode_t mode)
{
  lock(&b->sync);

  if (!b->has_data[subframe][slot][symbol]) {
    printf("warning: putting data in a place with no data %d.%d.%d\n", subframe, slot, symbol);
    unlock(&b->sync);
    return NULL;
  }

  while (b->rb_count[subframe][slot][symbol] == b->nb_rb) {
    if (mode == BUFFER_ACCESS_NO_WAIT) {
      printf("warning: cannot put data, dropped\n");
      unlock(&b->sync);
      return NULL;
    }
    sync_wait(&b->sync);
  }

  sync_notify(&b->sync);

  //int subframes_per_frame = 10;
  int re_per_rb = 12;
  int bytes_per_re = 4;

  unlock(&b->sync);

  char (*x)[b->slots_per_subframe][b->symbols_per_slot][b->nb_rb][re_per_rb][bytes_per_re] =
    (char (*)[b->slots_per_subframe][b->symbols_per_slot][b->nb_rb][re_per_rb][bytes_per_re])b->data;
//printf("x %p (ret %p) b->data %p subframes_per_frame %d b->slots_per_subframe %d b->symbols_per_slot %d b->nb_rb %d re_per_rb %d bytes_per_re %d %d.%d.%d\n", x, &x[subframe][slot][symbol], b->data, subframes_per_frame, b->slots_per_subframe, b->symbols_per_slot, b->nb_rb, re_per_rb, bytes_per_re, subframe, slot, symbol);
  return (char *)&x[subframe][slot][symbol];
}

/* call it after _get when done with buffer */
void buffer_release_data(buffer_config_t *b, int subframe, int slot, int symbol)
{
  lock(&b->sync);

  b->rb_count[subframe][slot][symbol] = 0;
  sync_notify(&b->sync);

  unlock(&b->sync);
}

/* call it after _put when done with buffer */
void buffer_commit_data(buffer_config_t *b, int subframe, int slot, int symbol, int rb_count)
{
  lock(&b->sync);

  b->rb_count[subframe][slot][symbol] += rb_count;
//if (b->rb_count[subframe][slot][symbol] > b->nb_rb) abort();
if (b->rb_count[subframe][slot][symbol] > b->nb_rb) { b->rb_count[subframe][slot][symbol] = b->nb_rb; }
  sync_notify(&b->sync);


  unlock(&b->sync);
}

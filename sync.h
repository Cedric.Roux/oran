#ifndef _SYNC_H_
#define _SYNC_H_

#include <pthread.h>

typedef struct {
  pthread_mutex_t m;
  pthread_cond_t c;
} sync_config_t;

void sync_init(sync_config_t *);
void lock(sync_config_t *);
void unlock(sync_config_t *);
void sync_wait(sync_config_t *);
void sync_notify(sync_config_t *);

#endif /* _SYNC_H_ */

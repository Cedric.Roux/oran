sudo sh -c 'echo 0 > /sys/class/net/enp1s0f0np0/device/sriov_numvfs'
sudo sh -c 'echo 3 > /sys/class/net/enp1s0f0np0/device/sriov_numvfs'
sudo ip link set enp1s0f0np0 vf 0 mac 76:76:64:6e:00:01 spoofchk off mtu 9600
sudo ip link set enp1s0f0np0 vf 0 vlan 4
sudo ip link set address 76:76:64:6e:00:01 dev enp1s0f0v0
sleep 3
sudo ifconfig enp1s0f0v0 up mtu 9600
sudo sysctl -w net.core.wmem_max=33554432
sudo sysctl -w net.core.rmem_max=33554432
sudo sysctl -w net.core.wmem_default=33554432
sudo sysctl -w net.core.rmem_default=33554432
sudo ethtool -G enp1s0f0v0 tx 8192 rx 8192
sudo ethtool -G enp1s0f0np0 tx 8192 rx 8192

CC=gcc
CFLAGS=-Wall -g -pthread
#CFLAGS=-Wall -g -pthread -fsanitize=thread

CFLAGS_LIB = $(CFLAGS) -fvisibility=hidden -fPIC

CFLAGS_LIB += -march=native

#for bronze
#OAI_DIR=/home/matix/roux/bronze/openairinterface5g
#CFLAGS_LIB += -D__ETHERNET_LIB_H__=\"radio/ETHERNET/USERSPACE/LIB/ethernet_lib.h\"

#for E
OAI_DIR=/home/matix/roux/E/openairinterface5g
CFLAGS_LIB += -D__ETHERNET_LIB_H__=\"radio/ETHERNET/ethernet_lib.h\"

CFLAGS_LIB += -I $(OAI_DIR) \
              -I $(OAI_DIR)/openair1 \
              -I $(OAI_DIR)/openair2/COMMON \
              -I $(OAI_DIR)/radio/COMMON \
              -I $(OAI_DIR)/nfapi/open-nFAPI/nfapi/public_inc \
              -I $(OAI_DIR)/common/utils

CFLAGS_LIB += -DMAX_NUM_CCs=1 \
              -DNB_ANTENNAS_RX=4

PROG=oran
LIB=liboran.so

OBJS=dl.o ul.o prach.o rx.o utils.o nr.o packetize.o \
     time_util.o buffers.o sync.o

OBJS_LIB=$(addsuffix _lib.o,$(basename $(OBJS)))

all: $(PROG) $(LIB)

$(PROG) : main.o $(OBJS)
	$(CC) $(CFLAGS) -o $@ $^ -lm

$(LIB) : lib_lib.o $(OBJS_LIB)
	$(CC) $(CFLAGS_LIB) -o $@ $^ -lm -shared

%.o: %.c
	$(CC) $(CFLAGS) -c $<

%_lib.o: %.c
	$(CC) $(CFLAGS_LIB) -c $< -o $@

clean:
	rm -f $(PROG) $(LIB) *.o

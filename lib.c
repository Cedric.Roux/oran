#include "utils.h"
#include "dl.h"
#include "ul.h"
#include "prach.h"
#include "rx.h"

#include "radio/COMMON/common_lib.h"
//#include "radio/ETHERNET/ethernet_lib.h"
#include __ETHERNET_LIB_H__

typedef struct {
  eth_state_t e;
  processor_config_t p;
} oran_native_eth_state_t;

int start(openair0_device *device)
{
  oran_native_eth_state_t *eth = device->priv;
  processor_config_t *p = &eth->p;
printf("XXX start device %p device->priv %p\n", device, device->priv); fflush(stdout);

  get_gps_start_time(&p->gps_start_time);
  //get_gps_time(&p.gps_start_time);

  new_thread(dl_thread, p);
  new_thread(ul_thread, p);
  new_thread(prach_thread, p);
  new_thread(rx_thread, p);

  return 0;
}

int oran_read(openair0_device *device,
                         openair0_timestamp *timestamp,
                         void **buff, int nsamps, int cc)
{
  /* not used */
  abort();
}

void south_in_record(RU_t *ru, int *_frame, int *_slot)
{
  static int frame = 0;
  static int slot = 0;

  RU_proc_t *proc = &ru->proc;

  extern uint16_t sl_ahead;

  proc->tti_rx = slot;
  proc->frame_rx = frame;
  proc->tti_tx = (slot + sl_ahead) % 20;
  proc->frame_tx = (slot > 19-sl_ahead) ? (frame + 1) & 1023 : frame;

  if (proc->first_tx) {
    *_frame = frame;
    *_slot = slot;
  }

  proc->first_rx = 0;

  usleep(50);
  *_frame = frame;
  *_slot = slot;
  slot++;
  if (slot == 20) {
    slot = 0;
    frame++;
    if (frame == 1024) frame = 0;
  }
}

void south_out_record(RU_t *ru, int frame, int slot, uint64_t timestamp)
{
  /* we record 1024 full frames, and gnb starts at slot!=0 so we do
   * a first loop to then start recording at 0.0
   */
  oran_native_eth_state_t *eth = ru->ifdevice.priv;
  int i;
  static int loop = 0;
  static int last_frame = -1;
  if (slot == 0 && frame == 0 && last_frame == 1023) loop++;
  last_frame = frame;
  static int nsyms = 0;
  static FILE *out = NULL;
  if (out == NULL) {
    out = fopen("/tmp/dump.raw", "w");
    if (out == NULL) abort();
  }
  int subframe = slot / 2;
  slot = slot % 2;
  int symbol;
  if (loop == 2) { printf("%d symbols dumped\n", nsyms); fclose(out); exit(1); }

  int start_symbol = eth->p.cfg.start_dl_symbol[subframe * 2 + slot];
  int nb_symbols =  eth->p.cfg.num_dl_symbol[subframe * 2 + slot];

  static char *b = NULL;
  if (b == NULL) {
    b = calloc(1, 273*12*4);
    if (b == NULL) abort();
  }

  for (symbol = start_symbol; symbol < start_symbol + nb_symbols; symbol++) {

    uint16_t *from;
    uint16_t *to;

    /* settings (hardcoded for now) */
    int sample_count = 273 * 12;
    int ant_id = 0;
    int fft_size = 4096;

    /* negative freqs */
    to = (uint16_t *)b;
    from = (uint16_t *)&ru->common.txdataF_BF[ant_id][symbol * 4096];
    from += (fft_size - sample_count / 2) * 2;

    for (i = 0; i < sample_count / 2; i++) {
      *to++ = htons(*from++);
      *to++ = htons(*from++);
    }

    /* positive freqs */
    from = (uint16_t *)&ru->common.txdataF_BF[ant_id][symbol * 4096];

    for (i = 0; i < sample_count / 2; i++) {
      *to++ = htons(*from++);
      *to++ = htons(*from++);
    }

    if (loop == 1) {
      fwrite(&frame, 4, 1, out);
      fwrite(&subframe, 4, 1, out);
      fwrite(&slot, 4, 1, out);
      fwrite(&symbol, 4, 1, out);
      fwrite(b, 273*12*4, 1, out);
      nsyms++;
    }
  }
}

void south_in(RU_t *ru, int *_frame, int *_slot)
{
  oran_native_eth_state_t *eth = ru->ifdevice.priv;
//printf("XXX ru %p ru->ifdevice %p ru->ifdevice.priv transport_init %p\n", ru, &ru->ifdevice, ru->ifdevice.priv); fflush(stdout);
  static int frame = 0;
  static int slot = 0;

if (frame % 100 == 0 && slot == 0) {
fprintf(stderr, "late %d\n", eth->p.ul->late_packets); fflush(stderr);
}

if (slot != *_slot || frame != *_frame) { printf("uhoh\n"); exit(1); }
#if 1
  int symbol;
  for (symbol = 0; symbol < 14; symbol++) {
//printf("YO south_in calling buffer_get_data %d.%d.%d\n", slot/2, slot%2, symbol);
    char *b = buffer_get_data(eth->p.ul, slot / 2, slot % 2, symbol, BUFFER_ACCESS_BLOCKING);
//printf("YO south_in returns from buffer_get_data %d.%d.%d b %p\n", slot/2, slot%2, symbol, b);
    if (b == NULL) { printf("fatal: buffer_get_data returns NULL\n"); exit(1); }
    if (b != (void *)1)
      buffer_release_data(eth->p.ul, slot / 2, slot % 2, symbol);
  }
#endif

  RU_proc_t *proc = &ru->proc;

  extern uint16_t sl_ahead;

  proc->tti_rx = slot;
  proc->frame_rx = frame;
  proc->tti_tx = (slot + sl_ahead) % 20;
  proc->frame_tx = (slot > 19-sl_ahead) ? (frame + 1) & 1023 : frame;

if (proc->first_tx)
{
*_frame = frame;
*_slot = slot;
}

  proc->first_rx = 0;

//if (frame == 0 && slot == 0)
//printf("YO south_in in frame %d slot %d local %d.%d proc rx %d.%d tx %d.%d proc %p size ru %ld size proc %ld\n", *_frame, *_slot, frame, slot, proc->frame_rx, proc->tti_rx, proc->frame_tx, proc->tti_tx, proc, sizeof(*ru), sizeof(*proc)); fflush(stdout);
//printf("in frame %d slot %d local %d.%d\n", *_frame, *_slot, frame, slot);
//usleep(50);
  *_frame = frame;
  *_slot = slot;
  slot++;
  if (slot == 20) {
    slot = 0;
    frame++;
    if (frame == 1024) frame = 0;
  }
}

void south_out(RU_t *ru, int frame, int slot, uint64_t timestamp)
{
  oran_native_eth_state_t *eth = ru->ifdevice.priv;
  int i;
//printf("YO south_out frame %d slot %d timestamp %ld\n", frame, slot, timestamp); fflush(stdout);
  int subframe = slot / 2;
  slot = slot % 2;
  int symbol;

  int start_symbol = eth->p.cfg.start_dl_symbol[subframe * 2 + slot];
  int nb_symbols =  eth->p.cfg.num_dl_symbol[subframe * 2 + slot];

  for (symbol = start_symbol; symbol < start_symbol + nb_symbols; symbol++) {
    char *b = buffer_put_data(eth->p.dl, subframe, slot, symbol, BUFFER_ACCESS_BLOCKING);
    if (b == NULL) { printf("fatal: buffer_put_data returns NULL\n"); exit(1); }

    uint16_t *from;
    uint16_t *to;

    /* settings (hardcoded for now) */
    int sample_count = 273 * 12;
    int ant_id = 0;
    int fft_size = 4096;

    /* negative freqs */
    to = (uint16_t *)b;
    from = (uint16_t *)&ru->common.txdataF_BF[ant_id][symbol * 4096];
    from += (fft_size - sample_count / 2) * 2;

    for (i = 0; i < sample_count / 2; i++) {
      *to++ = htons(*from++);
      *to++ = htons(*from++);
    }

    /* positive freqs */
    from = (uint16_t *)&ru->common.txdataF_BF[ant_id][symbol * 4096];

    for (i = 0; i < sample_count / 2; i++) {
      *to++ = htons(*from++);
      *to++ = htons(*from++);
    }

    buffer_commit_data(eth->p.dl, subframe, slot, symbol, 273);
  }
}

void *get_internal_parameter(char *name)
{
  printf("ORAN: %s\n", __FUNCTION__);

  if (getenv("RECORD")) {
    printf("RECORD mode\n");
    if (!strcmp(name, "fh_if4p5_south_in"))
      return (void *)south_in_record;
    if (!strcmp(name, "fh_if4p5_south_out"))
      return (void *)south_out_record;
  }

  if (!strcmp(name, "fh_if4p5_south_in"))
    return (void *)south_in;
  if (!strcmp(name, "fh_if4p5_south_out"))
    return (void *)south_out;

  return NULL;
}

#define ECPRI_PROTOCOL 0xae, 0xfe
#define DST_ADDR 0x98, 0xae, 0x71, 0x01, 0xc5, 0xeb
//#define SRC_ADDR  0x00, 0x11, 0x22, 0x33, 0x44, 0x66
#define SRC_ADDR  0x76, 0x76, 0x64, 0x6e, 0x00, 0x01

void init_cfg(packet_config_t *cfg)
{
  memcpy(cfg->eth_dst, (char[]){ DST_ADDR }, 6);
  memcpy(cfg->eth_src, (char[]){ SRC_ADDR }, 6);
  memcpy(cfg->eth_type, (char[]){ ECPRI_PROTOCOL }, 2);
  cfg->vlan = -1;
  cfg->eaxc_id_dl = 0;
  cfg->eaxc_id_ul = 0;
  cfg->eaxc_id_prach = 2;

  static int start_dl_symbol[20] = {
    0, 0, 0, 0, -1,
    0, 0, 0, 0, -1,
    0, 0, 0, 0, -1,
    0, 0, 0, 0, -1,
  };

  static int num_dl_symbol[20] = {
    14, 14, 14, 6, -1,
    14, 14, 14, 6, -1,
    14, 14, 14, 6, -1,
    14, 14, 14, 6, -1,
  };

  cfg->start_dl_symbol = start_dl_symbol;
  cfg->num_dl_symbol = num_dl_symbol;

  static int start_ul_symbol[20] = {
    -1, -1, -1, 10, 0,
    -1, -1, -1, 10, 0,
    -1, -1, -1, 10, 0,
    -1, -1, -1, 10, 0,
  };

  static int num_ul_symbol[20] = {
    -1, -1, -1, 4, 14,
    -1, -1, -1, 4, 14,
    -1, -1, -1, 4, 14,
    -1, -1, -1, 4, 14,
  };

  cfg->start_ul_symbol = start_ul_symbol;
  cfg->num_ul_symbol = num_ul_symbol;

  static int prach_slot[20] = {
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 1,
  };

  cfg->prach_slot = prach_slot;

  cfg->slots_per_subframe = 2;
}

void init(processor_config_t *p, char *iface)
{
  init_cfg(&p->cfg);

  p->mu = 1;
  p->mtu = 9000;
  /* DL timings */
  p->tadv_cp_dl    = 125 * 1000;
  p->t1a_max_cp_dl = 470 * 1000;
  p->t1a_max_up    = 350 * 1000;
  /* UL timings */
  p->t1a_max_cp_ul = 429 * 1000;
  /* PRACH timings */
  p->t2a_max_cp_ul = 429 * 1000;

  unsigned char prot[2] = { ECPRI_PROTOCOL };
  int s;
  int interface_index;

  unsigned short ecpri_prot = (prot[1] << 8) | prot[0];

  s = socket(AF_PACKET, SOCK_RAW, ecpri_prot);
  if (s == -1) { perror("socket"); exit(1); }

  struct ifreq itf;
  if (strlen(iface) > IFNAMSIZ-1) { printf("interface name %s too long\n", iface); exit(1); }
  strcpy(itf.ifr_name, iface);

  if (ioctl(s, SIOCGIFINDEX, &itf) == -1) { perror("ioctl"); exit(1); }
  interface_index = itf.ifr_ifindex;
  printf("interface %s has index %d\n", iface, interface_index);

  struct sockaddr_ll addr;
  addr.sll_family = AF_PACKET;
  addr.sll_protocol = htons(0xaefe);
  addr.sll_ifindex = interface_index;

  if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) == -1) { perror("bind"); exit(1); }

  p->socket = s;
  p->interface_index = interface_index;
}

void init_buffers(processor_config_t *p)
{
  int subframe, slot, symbol;

printf("dl config:\n");
  /* dl */
  p->dl = new_buffer(2, 14, 273);
  for (subframe = 0; subframe < 10; subframe++)
    for (slot = 0; slot < 2; slot++) {
      int sl = subframe * 2 + slot;
      int start = p->cfg.start_dl_symbol[sl];
      int end = p->cfg.start_dl_symbol[sl] + p->cfg.num_dl_symbol[sl] - 1;
      for (symbol = start; symbol <= end; symbol ++)
        buffer_set_has_data(p->dl, subframe, slot, symbol);
    }

printf("ul config:\n");
  /* ul */
  p->ul = new_buffer(2, 14, 273);
  for (subframe = 0; subframe < 10; subframe++)
    for (slot = 0; slot < 2; slot++) {
      int sl = subframe * 2 + slot;
      int start = p->cfg.start_ul_symbol[sl];
      int end = p->cfg.start_ul_symbol[sl] + p->cfg.num_ul_symbol[sl] - 1;
      for (symbol = start; symbol <= end; symbol ++)
        buffer_set_has_data(p->ul, subframe, slot, symbol);
    }
}

__attribute__((__visibility__("default")))
int transport_init(openair0_device *device,
                   openair0_config_t *openair0_cfg,
                   eth_params_t * eth_params )
{
  oran_native_eth_state_t *eth;

  eth = calloc(1, sizeof(*eth));
  AssertFatal(eth != NULL, "out of memory\n");

  eth->e.flags = ETH_RAW_IF4p5_MODE;
  eth->e.compression = NO_COMPRESS;
  eth->e.if_name = eth_params->local_if_name;

  device->transp_type = ETHERNET_TP;
  device->priv = eth;
printf("XXX device %p device->priv transport_init %p\n", device, device->priv); fflush(stdout);
  device->openair0_cfg = &openair0_cfg[0];

  device->trx_start_func = start;
  device->trx_read_func  = oran_read;
  device->get_internal_parameter = get_internal_parameter;

  device->Mod_id               = 0;
  device->trx_get_stats_func   = (void *)10;
  device->trx_reset_stats_func = (void *)10;
  device->trx_end_func         = (void *)10;
  device->trx_stop_func        = (void *)10;
  device->trx_set_freq_func    = (void *)10;
  device->trx_set_gains_func   = (void *)10;
  device->trx_write_func       = (void *)10;
  device->trx_ctlsend_func     = (void *)10;
  device->trx_ctlrecv_func     = (void *)10;

  init(&eth->p, "enp1s0f0v2");

  init_buffers(&eth->p);

  return 0;
}

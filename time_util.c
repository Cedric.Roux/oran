#include "time_util.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* as of writing: utc to ptp is +37 (ie. ptp time - utc time = +37 seconds */
/* and utc to gps is +18s */
static int utc_to_gps = +18;

void get_gps_time(gps_time_t *now)
{
  struct timespec t;
  if (clock_gettime(CLOCK_REALTIME, &t)) { perror("clock_gettime"); exit(1); }
  now->second = t.tv_sec + utc_to_gps;
  now->nanosecond = t.tv_nsec;
}

gps_time_t add_nanosecond_gps_time(gps_time_t t, int nanosec)
{
  gps_time_t ret;
  ret.nanosecond = t.nanosecond + nanosec;
  ret.second = t.second + ret.nanosecond / 1000000000;
  ret.nanosecond %= 1000000000;
  return ret;
}

gps_time_t sub_nanosecond_gps_time(gps_time_t t, int nanosec)
{
  return timediff(t, (gps_time_t){ nanosec / 1000000000, nanosec % 1000000000 });
}

gps_time_t timediff(gps_time_t a, gps_time_t b)
{
  gps_time_t ret;

  ret.second = a.second - b.second;

  if (a.nanosecond > b.nanosecond) {
    ret.nanosecond = a.nanosecond - b.nanosecond;
  } else {
    ret.nanosecond = a.nanosecond - b.nanosecond + 1000000000;
    ret.second--;
  }

  return ret;
}

void gps_nanosleep(gps_time_t abstime)
{
  struct timespec t;
  t.tv_sec = abstime.second - utc_to_gps;
  t.tv_nsec = abstime.nanosecond;
  if (clock_nanosleep(CLOCK_REALTIME, TIMER_ABSTIME, &t, NULL))
    { perror("clock_nanosleep"); exit(1); }
}

void get_gps_start_time(gps_time_t *start)
{
  /* get next transmit time for frame/subframe/slot 0/0/0 */
  /* O-RAN.WG4.CUS.0-R003-v12.00 11.7.2 says:
   * frame_number = floor((gps_second - beta * 0.01 - alpha / 1.2288e9)
   *                      / frame_period_in_seconds)
   *                mod (max_frame_number + 1)
   * with:
   * frame_period_in_seconds = 0.01 seconds
   * max_frame_number = 1023
   * alpha and beta configurable, let's use 0
   */

  gps_time_t now;
  get_gps_time(&now);
  uint64_t t = now.second * 100 + now.nanosecond / 10000000;
  t += 1023;
  t &= ~((uint64_t)1023);
  start->second = t / 100; // + 1;
  start->nanosecond = (t % 100) * 10000000;
//start->nanosecond += 1000000 + 600000;
if (start->nanosecond > 1000000000) {
  start->nanosecond -= 1000000000;
  start->second++;
}
}

void gps_to_ptp(ptp_time_t *out, gps_time_t *in)
{
  /* IEEE 1588-2008 Annex B Table B.1: GPS Seconds = PTP Seconds - 315 964 819 */
  out->second = in->second + 315964819;
  out->nanosecond = in->nanosecond;
}

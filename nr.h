#ifndef _NR_H_
#define _NR_H_

double n_cp_l_mu(int mu, int l, int is_extended);
double t_start_l_mu(int mu, int l, int is_extended);

#endif /* _NR_H_ */

#include "packetize.h"

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "utils.h"

#define PUT(b, l) do { \
    if (ret + (l) > size) { printf("buffer too small\n"); return -1; } \
    memcpy(out+ret, (b), (l)); \
    ret += (l); \
  } while (0)

#define PUT_BIT(b) do { \
    if (ret == size) { printf("buffer too small\n"); return -1; } \
    out[ret] &= ~((1) << (7 - bit)); \
    out[ret] |= (b) << (7 - bit); \
    bit++; \
    if (bit == 8) { \
      bit = 0; \
      ret++; \
    } \
  } while (0)

#define PUT_BITS(v, l) do { \
    for (int i = 0; i < (l); i++) \
      PUT_BIT(((v) >> ((l) - 1 - i))&1); \
  } while (0)

static int put_ethernet_header(packet_config_t *cfg, char *out, int size)
{
  int ret = 0;

  PUT(cfg->eth_dst, 6);
  PUT(cfg->eth_src, 6);

  if (cfg->vlan != -1) {
    char vlan[4] = { 0x81, 0x00, cfg->vlan / 256, cfg->vlan % 256 };
    PUT(vlan, 4);
  }

  PUT(cfg->eth_type, 2);

  return ret;
}

static int make_cp_uldl(packet_config_t *cfg, packet_state_t *st, char *out, int size, bool dl)
{
  int ret = 0;
  int bit = 0;
  int data_start;
  int payload_size;
  char *out_payload_size;

  ret = put_ethernet_header(cfg, out, size);
  if (ret == -1)
    return -1;

  /* ecpri */

  PUT_BITS(1, 4);  /* ecpriVersion */
  PUT_BITS(0, 3);  /* ecpriReserved */
  PUT_BITS(0, 1);  /* ecpriConcatenation */

  PUT_BITS(0x02, 8);  /* ecpriMessage 2 (realtime control data) */

  out_payload_size = out + ret;

  PUT_BITS(0, 16);  /* ecpri payload size put at the end */

  data_start = ret;

  PUT_BITS(cfg->eaxc_id_dl, 16);
  PUT_BITS(dl ? st->seq_id_cp_dl : st->seq_id_cp_ul, 8);

  PUT_BITS(1, 1);     /* E bit, always 1 */
  PUT_BITS(0, 7);     /* subsequence ID, always 0 */

  /* oran */
  PUT_BITS(dl ? 1 : 0, 1);     /* data direction: 1 = DL, 0 = UL */
  PUT_BITS(1, 3);              /* payload version: 1 */
  PUT_BITS(0, 4);              /* filter index 0 (see oran CUS v12 7.5.2.3) */

  PUT_BITS(st->frame, 8);
  PUT_BITS(st->subframe, 4);
  PUT_BITS(st->slot, 6);
  int start_symbol;
  if (dl)
    start_symbol  = cfg->start_dl_symbol[st->subframe * cfg->slots_per_subframe + st->slot];
  else
    start_symbol  = cfg->start_ul_symbol[st->subframe * cfg->slots_per_subframe + st->slot];
  if (start_symbol == -1) { fprintf(stderr, "error: start symbol is -1, not %s slot?\n", dl ? "DL" : "UL"); return -1; }
  PUT_BITS(start_symbol, 6);    /* start symbol */

  PUT_BITS(1, 8);               /* number of sections: 1 */
  PUT_BITS(1, 8);               /* session type: 1 */
  PUT_BITS(0, 8);               /* udcompheader: 0  (TODO: no hardcode) */
  PUT_BITS(0, 8);               /* reserved: 0 */

  PUT_BITS(0, 12);              /* section ID: 0 (TODO: be sure, see 7.5.3.1) */
  PUT_BITS(0, 1);               /* rb indicator: all rbs */
  PUT_BITS(0, 1);               /* syminc 0 */
  PUT_BITS(0, 10);              /* start prb 0 */
  PUT_BITS(0, 8);               /* number of prb 0 (means all rbs) */
  PUT_BITS(0xfff, 12);          /* re mask, all 1 */

  int num_symbols;
  if (dl)
    num_symbols = cfg->num_dl_symbol[st->subframe * cfg->slots_per_subframe + st->slot];
  else
    num_symbols = cfg->num_ul_symbol[st->subframe * cfg->slots_per_subframe + st->slot];
  if (num_symbols == -1) { fprintf(stderr, "error: num symbols is -1, not %s slot?\n", dl ? "DL" : "UL"); return -1; }
  PUT_BITS(num_symbols, 4);     /* number of symbols */

  PUT_BITS(0, 1);               /* ef = 0 (no extension) */
  PUT_BITS(0, 15);              /* beam id */

  /* put payload size */
  payload_size = ret - data_start;
  out_payload_size[0] = payload_size >> 8;
  out_payload_size[1] = payload_size & 255;

  return ret;
}

int make_cp_dl(packet_config_t *cfg, packet_state_t *st, char *out, int size)
{
  return make_cp_uldl(cfg, st, out, size, true);
}

int make_cp_ul(packet_config_t *cfg, packet_state_t *st, char *out, int size)
{
  return make_cp_uldl(cfg, st, out, size, false);
}

int make_up_dl(packet_config_t *cfg, packet_state_t *st, char *out, int size,
               char *iq, int rb_start, int rb_count, int symbol_id)
{
  int ret = 0;
  int bit = 0;
  int data_start;
  int payload_size;
  char *out_payload_size;

  ret = put_ethernet_header(cfg, out, size);
  if (ret == -1)
    return -1;

  /* ecpri */

  PUT_BITS(1, 4);  /* ecpriVersion */
  PUT_BITS(0, 3);  /* ecpriReserved */
  PUT_BITS(0, 1);  /* ecpriConcatenation */

  PUT_BITS(0x00, 8);  /* ecpriMessage 0 (IQ data) */

  out_payload_size = out + ret;

  PUT_BITS(0, 16);  /* ecpri payload size put at the end */

  data_start = ret;

  PUT_BITS(cfg->eaxc_id_dl, 16);
  PUT_BITS(st->seq_id_up_dl, 8);

  PUT_BITS(1, 1);     /* E bit, always 1 */
  PUT_BITS(0, 7);     /* subsequence ID, always 0 */

  /* oran */
  PUT_BITS(1, 1);     /* data direction: 1 = DL */
  PUT_BITS(1, 3);     /* payload version: 1 */
  PUT_BITS(0, 4);     /* filter index 0 (see oran CUS v12 7.5.2.3) */

  PUT_BITS(st->frame, 8);
  PUT_BITS(st->subframe, 4);
  PUT_BITS(st->slot, 6);
  PUT_BITS(symbol_id, 6);

  PUT_BITS(0, 12);              /* section ID: 0 (TODO: be sure, see 7.5.3.1) */
  PUT_BITS(0, 1);               /* rb indicator: all rbs */
  PUT_BITS(0, 1);               /* syminc 0 */
  PUT_BITS(rb_start, 10);       /* start prb */
  PUT_BITS(rb_count, 8);        /* number of prb */

  /* IQ */
  if (cfg->compression_type != COMPRESSION_NONE_16BITS) {
    fprintf(stderr, "unsupported compression\n");
    return -1;
  }

  int iq_size = rb_count * 12 * 4;

  if (ret + iq_size > size) { printf("buffer too small\n"); return -1; }

  memcpy(out+ret, iq, iq_size);

  ret += iq_size;

  /* put payload size */
  payload_size = ret - data_start;
  out_payload_size[0] = payload_size >> 8;
  out_payload_size[1] = payload_size & 255;

  return ret;
}

int make_oneway_delay(packet_config_t *cfg, char *out, int size, gps_time_t *now)
{
  int ret = 0;
  int bit = 0;
  int data_start;
  int payload_size;
  char *out_payload_size;

  ret = put_ethernet_header(cfg, out, size);
  if (ret == -1)
    return -1;

  /* ecpri */

  PUT_BITS(1, 4);  /* ecpriVersion */
  PUT_BITS(0, 3);  /* ecpriReserved */
  PUT_BITS(0, 1);  /* ecpriConcatenation */

  PUT_BITS(0x05, 8);  /* ecpriMessage 5 (one way measurement) */

  out_payload_size = out + ret;

  PUT_BITS(0, 16);  /* ecpri payload size put at the end */

  data_start = ret;

  PUT_BITS(0, 8);    /* measurement ID */
  PUT_BITS(0, 8);    /* action type: 0 (request) */

  ptp_time_t ptp;
  gps_to_ptp(now, &ptp);

  PUT_BITS(ptp.second, 48);
  PUT_BITS(ptp.nanosecond, 32);

  PUT_BITS(0, 32);  /* compensation high 32 bits: 0 */
  PUT_BITS(0, 32);  /* compensation low 32 bits: 0 */

  /* 1000 dummy uninitialized bytes */
  ret += 1000;

  /* put payload size */
  payload_size = ret - data_start;
  out_payload_size[0] = payload_size >> 8;
  out_payload_size[1] = payload_size & 255;

  return ret;
}

int make_cp_prach(packet_config_t *cfg, packet_state_t *st, char *out, int size)
{
  int ret = 0;
  int bit = 0;
  int data_start;
  int payload_size;
  char *out_payload_size;

  ret = put_ethernet_header(cfg, out, size);
  if (ret == -1)
    return -1;

  /* ecpri */

  PUT_BITS(1, 4);  /* ecpriVersion */
  PUT_BITS(0, 3);  /* ecpriReserved */
  PUT_BITS(0, 1);  /* ecpriConcatenation */

  PUT_BITS(0x02, 8);  /* ecpriMessage 2 (realtime control data) */

  out_payload_size = out + ret;

  PUT_BITS(0, 16);  /* ecpri payload size put at the end */

  data_start = ret;

  PUT_BITS(cfg->eaxc_id_prach, 16);
  PUT_BITS(st->seq_id_cp_prach, 8);

  PUT_BITS(1, 1);     /* E bit, always 1 */
  PUT_BITS(0, 7);     /* subsequence ID, always 0 */

  /* oran */
  PUT_BITS(0, 1);              /* data direction: 0 = UL */
  PUT_BITS(1, 3);              /* payload version: 1 */
  PUT_BITS(3, 4);              /* filter index 3 (see oran CUS v12 7.5.2.3) (TODO: no hardcode) */

  PUT_BITS(st->frame, 8);
  PUT_BITS(st->subframe, 4);
  PUT_BITS(st->slot, 6);
  PUT_BITS(0, 6);               /* start symbol (TODO: no hardcode) */

  PUT_BITS(1, 8);               /* number of sections: 1 */
  PUT_BITS(3, 8);               /* session type: 3 */

  PUT_BITS(468, 16);            /* time offset (TODO: no hardcode) */
  PUT_BITS(0x0c, 4);            /* FFT size (TODO: no hardcode) */
  PUT_BITS(1, 4);               /* scs (TODO: no hardcode) */
  PUT_BITS(0, 16);              /* CP lenght (TODO: no hardcode) */
  PUT_BITS(0, 8);               /* udcompheader: 0  (TODO: no hardcode) */

  PUT_BITS(st->frame, 12);      /* section ID: the frame */
  PUT_BITS(0, 1);               /* rb indicator: all rbs */
  PUT_BITS(0, 1);               /* syminc 0 */
  PUT_BITS(0, 10);              /* start prb 0 */
  PUT_BITS(12, 8);              /* number of prb (TODO: no hardcode) */
  PUT_BITS(0xfff, 12);          /* re mask, all 1 */
  PUT_BITS(12, 4);              /* number of symbols (TODO: no hardcode) */
  PUT_BITS(0, 1);               /* ef = 0 (no extension) */
  PUT_BITS(0, 15);              /* beam id */
  PUT_BITS(0, 24);              /* freq offset (TODO: no hardcode) */
  PUT_BITS(0, 8);               /* reserved: 0 */

  /* put payload size */
  payload_size = ret - data_start;
  out_payload_size[0] = payload_size >> 8;
  out_payload_size[1] = payload_size & 255;

  return ret;
}

#define GET(b, l) do { \
    if (ret + (l) > size) { printf("buffer too small\n"); return -1; } \
    memcpy((b), in+ret, (l)); \
    ret += (l); \
  } while (0)

#define GET_BIT(b) do { \
    if (ret == size) { printf("buffer too small\n"); return -1; } \
    (b) |= (in[ret] >> (7 - bit)) & 1; \
    bit++; \
    if (bit == 8) { \
      bit = 0; \
      ret++; \
    } \
  } while (0)

#define GET_BITS(v, l) do { \
    (v) = 0; \
    for (int i = 0; i < (l); i++) { \
      (v) <<= 1; \
      GET_BIT((v)); \
    } \
  } while (0)

#define GET_BITS_AND_CHECK(v, l) do { \
    int val; \
    GET_BITS(val, l); \
    if (val != (v)) return -1; \
  } while (0)

int depacketize(packet_config_t *cfg, char *in, int size, depacket_state_t *s)
{
  char dst[6];
  char src[6];
  char eth_type[2];
  int ret = 0;
  int bit = 0;

  GET(dst, 6);
  GET(src, 6);

  if (cfg->vlan != -1) {
    char vlan[4]; // = { 0x81, 0x00, cfg->vlan / 256, cfg->vlan % 256 };
    GET(vlan, 4);
    int vlan_val = (((unsigned char)vlan[2]) << 8) | (unsigned char)vlan[3];
    if (vlan[0] != 0x81 || vlan[1] != 0
        || vlan_val != cfg->vlan)
      return 0;
  }

  GET(eth_type, 2);

  /* it's a reply so src/dst are reversed */
  if (memcmp(dst, cfg->eth_src, 6)
      || memcmp(src, cfg->eth_dst, 6)
      || memcmp(eth_type, cfg->eth_type, 2))
    return -1;

  /* ecpri */

  GET_BITS_AND_CHECK(1, 4);  /* ecpriVersion */
  GET_BITS_AND_CHECK(0, 3);  /* ecpriReserved */
  GET_BITS_AND_CHECK(0, 1);  /* ecpriConcatenation */

  int message_type;
  GET_BITS(message_type, 8);  /* ecpriMessage 0 (IQ data) */

  int payload_size;
  GET_BITS(payload_size, 16);  /* ecpri payload size */

  int ecpri_id;
  int ecpri_seq_id;
  GET_BITS(ecpri_id, 16);
  GET_BITS(ecpri_seq_id, 8);
//printf("recv message_type %d ecpri_id %d\n", message_type, ecpri_id);

  GET_BITS_AND_CHECK(1, 1);     /* E bit, always 1 */
  GET_BITS_AND_CHECK(0, 7);     /* subsequence ID, always 0 */

  /* oran */
  if (message_type != 0)
    return -1;

  GET_BITS_AND_CHECK(0, 1);     /* data direction: 0 = UL */
  GET_BITS_AND_CHECK(1, 3);     /* payload version: 1 */
  int filter_index;
  GET_BITS(filter_index, 4);     /* filter index (see oran CUS v12 7.5.2.3) */

  int frame, subframe, slot, symbol;
  GET_BITS(frame, 8);
  GET_BITS(subframe, 4);
  GET_BITS(slot, 6);
  GET_BITS(symbol, 6);

  int section_id;
  GET_BITS(section_id, 12);               /* section ID (see oran CUS v12 7.5.3.1) */
  GET_BITS_AND_CHECK(0, 1);               /* rb indicator: all rbs */
  GET_BITS_AND_CHECK(0, 1);               /* syminc 0 */

  int rb_start, rb_count;
  GET_BITS(rb_start, 10);       /* start prb */
  GET_BITS(rb_count, 8);        /* number of prb */
//  printf("recv f.sf.slot.symb %d.%d.%d.%d rb start/end %d.%d filter_index %d section_id %d\n", frame, subframe, slot, symbol, rb_start, rb_count, filter_index, section_id);

  /* todo: better way to get the type, filter_index may not work for all prach formats */
  s->type = filter_index == 0 ? PACKET_TYPE_UL : PACKET_TYPE_PRACH;
  s->frame = frame;
  s->subframe = subframe;
  s->slot = slot;
  s->symbol = symbol;
  s->rb_start = rb_start;
  s->rb_count = rb_count;
  s->data = in + ret;

  int nb_re = 12;
  int bytes_per_sample = 4;
  if (size - ret < rb_count * nb_re * bytes_per_sample)
    return -1;

  return 0;
}
